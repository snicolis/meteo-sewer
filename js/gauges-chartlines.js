Highcharts.setOptions({
    global: {
        useUTC: false
    }
});


var gauge1 = {
    angle: 0,
    lineWidth: 0.2,
    limitMax: 'false', 
    strokeColor: 'red',
    radiusScale: 1,
    generateGradient: true,
      pointer: {
      length: 0.4, // // Relative to gauge radius
      strokeWidth: 0.022, // The thickness
      color: 'black' // Fill color
    },
    staticLabels: {
    font: "80% sans-serif",  // Specifies font
    labels: [0, 3, 4, 6, 8, 10],  // Print labels at these values
    color: "black",  // Optional: Label text color
    fractionDigits: 0  // Optional: Numerical precision. 0=round off.
  },  // just experiment with them
    strokeColor: 'red',   // to see which ones work best for you
    staticZones: [
     {strokeStyle: "#C43933", min: 0, max: 2.95}, // red
     {strokeStyle: "#EF8636", min: 3.05, max: 3.95}, // orange
     {strokeStyle: "#DDDF4B", min: 4.05, max: 5.95}, // Yellow
     {strokeStyle: "#519E3E", min: 6.05, max: 7.95}, 
     // Green
      {strokeStyle: "#3B75AF", min: 8.05, max: 10}, 
     // Blue
  ],
    generateGradient: 'false'
  };
var gauge2 = {
    angle: 0,
    lineWidth: 0.2,
    limitMax: 'false', 
    limitMin: 'false', 

    strokeColor: 'red',
    radiusScale: 1,
    generateGradient: false,
      pointer: {
      length: 0.4, // // Relative to gauge radius
      strokeWidth: 0.022, // The thickness
      color: 'black' // Fill color
    },
    staticLabels: {
    font: "80% sans-serif",  // Specifies font
    labels: [4.5, 5.5, 6, 6.5, 8.5, 9, 9.5, 10.5],  // Print labels at these values
    color: "black",  // Optional: Label text color
    fractionDigits: 1  // Optional: Numerical precision. 0=round off.
  },  // just experiment with them
    strokeColor: 'red',   // to see which ones work best for you
    staticZones: [
    {strokeStyle: "#EF8636", min: 4.5, max: 5.48}, // orange
    {strokeStyle: "#DDDF4B", min: 5.52, max: 5.98}, // Yellow
    {strokeStyle: "#519E3E", min: 6.02, max: 6.48}, // Green
    {strokeStyle: "#3B75AF", min: 6.52, max: 8.48}, // Blue
    {strokeStyle: "#519E3E", min: 8.52, max: 8.98}, // Green
    {strokeStyle: "#DDDF4B", min: 9.02, max: 9.48}, // Yellow
    {strokeStyle: "#EF8636", min: 9.52, max: 10.5}, // orange



  ],
    generateGradient: 'false'
  };

var gauge3 = {
angle: 0,
lineWidth: 0.2,
limitMax: 'false', 
strokeColor: 'red',
radiusScale: 1,
generateGradient: true,
    pointer: {
    length: 0.4, // // Relative to gauge radius
    strokeWidth: 0.022, // The thickness
    color: 'black' // Fill color
},
staticLabels: {
font: "80% sans-serif",  // Specifies font
labels: [0, 23, 25, 30, 35],  // Print labels at these values
color: "black",  // Optional: Label text color
fractionDigits: 0  // Optional: Numerical precision. 0=round off.
},  // just experiment with them
strokeColor: 'red',   // to see which ones work best for you
staticZones: [
    {strokeStyle: "#3B75AF", min: 0, max: 22.95},     // Blue
    {strokeStyle: "#519E3E", min: 23.05, max: 24.95}, // Green
    {strokeStyle: "#DDDF4B", min: 25.05, max: 27.45}, // Yellow
    {strokeStyle: "#EF8636", min: 27.55, max: 29.95}, // orange
    {strokeStyle: "#C43933", min: 30.05, max: 35}, // red
],
generateGradient: 'false'
};

var gauge4 = {
    angle: 0,
    lineWidth: 0.2,
    limitMax: 'false', 
    strokeColor: 'red',
    radiusScale: 1,
    generateGradient: true,
        pointer: {
        length: 0.4, // // Relative to gauge radius
        strokeWidth: 0.022, // The thickness
        color: 'black' // Fill color
    },
    staticLabels: {
    font: "80% sans-serif",  // Specifies font
    labels: [0, 675, 900, 1125, 1350, 1600],  // Print labels at these values
    color: "black",  // Optional: Label text color
    fractionDigits: 0  // Optional: Numerical precision. 0=round off.
    },  // just experiment with them
    strokeColor: 'red',   // to see which ones work best for you
    staticZones: [
        {strokeStyle: "#3B75AF", min: 0, max: 674},     // Blue
        {strokeStyle: "#519E3E", min: 676, max: 899}, // Green
        {strokeStyle: "#DDDF4B", min:901, max: 1124}, // Yellow
        {strokeStyle: "#EF8636", min: 1126, max: 1349}, // orange
        {strokeStyle: "#C43933", min: 1351, max: 1600}, // red
    ],
    generateGradient: 'false'
    };


var target1 = document.getElementById('container-ox1'); 
var target2 = document.getElementById('container-ph1'); 
var target3 = document.getElementById('container-temp1'); 
var target4 = document.getElementById('container-cond1'); 

// gauge ox
var gaugeox = new Gauge(target1).setOptions(gauge1); 
gaugeox.minValue = 0; 
gaugeox.maxValue = 10;
gaugeox.animationSpeed = 32; 
var target1 = document.getElementById('container-ox1');
gaugeox.set(null);
gaugeox.setTextField(document.getElementById("gauge1-txt"),2);

// gauge ph
var gaugeph = new Gauge(target2).setOptions(gauge2); 
gaugeph.minValue = 4.5; // set max gauge value
gaugeph.maxValue = 10.5; // set max gauge value
gaugeph.animationSpeed = 32; 
var target2 = document.getElementById('container-ph1'); 
gaugeph.set(null);
gaugeph.setTextField(document.getElementById("gauge2-txt"), 2);

// gauge temp
var gaugete = new Gauge(target3).setOptions(gauge3); 
gaugete.minValue = 0; // set max gauge value
gaugete.maxValue = 35; // set max gauge value
gaugete.animationSpeed = 32; 
var target3 = document.getElementById('container-temp1'); // your canvas element
gaugete.set(null);
gaugete.setTextField(document.getElementById("gauge3-txt"), 2);

// gauge cond
var gaugeco = new Gauge(target4).setOptions(gauge4);
gaugeco.minValue = 0; // set max gauge value
gaugeco.maxValue = 1600; // set max gauge value
gaugeco.animationSpeed = 32; 
var target3 = document.getElementById('container-cond1'); // your canvas element
gaugeco.set(null);
gaugeco.setTextField(document.getElementById("gauge4-txt"), 2);



// var gaugeOptions2 = {
//     chart: {
//         type: 'solidgauge',
//         backgroundColor: '#ffffff00',
//         marginTop: 0,
//         marginBottom: 0,
//         marginLeft: 0,
//         marginRight: 0,
//         height: '66%',
//         // height: 270,
//         // width: 470,
//     },
//     title: null,
//     pane: {
//         center: ['50%', '70%'],
//         size: '110%',
//         startAngle: -90,
//         endAngle: 90,
//         background: {
//             backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#e5e5e5',
//             innerRadius: '60%',
//             outerRadius: '90%',
//             shape: 'arc'
//         }
//     },
//     exporting: {
//         enabled: false
//     },
//     tooltip: {
//         enabled: false
//     },
//     yAxis: {
//         plotBands: [{
//                 from: 4.5,
//                 to: 5.5,
//                 color: '#ff7f0e'
//             },
//             {
//                 from: 5.5,
//                 to: 6,
//                 color: '#DDDF0D'
//             },
//             {
//                 from: 6,
//                 to: 6.5,
//                 color: '#2ca02c'
//             },
//             {
//                 from: 6.5,
//                 to: 8.5,
//                 color: '#1f77b4'
//             },
//             {
//                 from: 8.5,
//                 to: 9,
//                 color: '#2ca02c'
//             },
//             {
//                 from: 9,
//                 to: 9.5,
//                 color: '#DDDF0D'
//             },
//             {
//                 from: 9.5,
//                 to: 10.5,
//                 color: '#ff7f0e'
//             }
//         ],

//         stops: [
//             [0., '#ff7f0e'], // red
//             [0.99/6 , '#ff7f0e'], // red
//             [1/6, '#DDDF0D'], // orange
//             [1.49/6, '#DDDF0D'], //orange
//             [1.5/6, '#2ca02c'], // yellow
//             [1.99/6, '#2ca02c'], // yellow
//             [2/6, '#1f77b4'], // green
//             [3.99/6, '#1f77b4'], // green
//             [4/6, '#2ca02c'], // blue
//             [4.49/6, '#2ca02c'], // blue
//             [4.5/6, '#DDDF0D'], // orange
//             [4.99/6, '#DDDF0D'], //orange
//             [5/6, '#ff7f0e'], // red
//             [1, '#ff7f0e'], // red

//         ],
//         lineWidth: 0,
//         tickWidth: 0,
//         minorTickInterval: null,
//         tickAmount: 2,
//         title: {
//             y: 0
//         },
//         labels: {
//             y: 0
//         },
//     },
//     plotOptions: {
//         solidgauge: {
//             innerRadius: '60%',
//             radius: '90%',
//             dataLabels: {
//                 y: 18,
//                 borderWidth: 0,
//                 useHTML: true
//             }
//         }
//     }
// }; //pH

// var gaugeOptions3 = {
//     chart: {
//         type: 'solidgauge',
//         backgroundColor: '#ffffff00',
//         marginTop: 0,
//         marginBottom: 0,
//         marginLeft: 0,
//         marginRight: 0,
//         height: '66%',
//     },
//     title: null,
//     pane: {
//         center: ['50%', '70%'],
//         size: '110%',
//         startAngle: -90,
//         endAngle: 90,
//         background: {
//             backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#e5e5e5',
//             innerRadius: '60%',
//             outerRadius: '90%',
//             shape: 'arc'
//         }
//     },
//     exporting: {
//         enabled: false
//     },
//     tooltip: {
//         enabled: false
//     },
//     yAxis: {
//         plotBands: [{
//                 from: 0,
//                 to: 23,
//                 color: '#1f77b4'
//             },
//             {
//                 from: 23,
//                 to: 25,
//                 color: '#2ca02c'
//             },
//             {
//                 from: 25,
//                 to: 27.5,
//                 color: '#DDDF0D'
//             },
//             {
//                 from: 27.5,
//                 to: 30,
//                 color: '#ff7f0e'
//             },
//             {
//                 from: 30,
//                 to: 35,
//                 color: '#d62728'
//             },

//         ],
//         stops: [
//             [0., '#1f77b4'], // blue
//             [(22.9) / 35, '#1f77b4'], // blue
//             [(23) / 35, '#2ca02c'], // green
//             [(24.9) / 35, '#2ca02c'], // green
//             [(25) / 35, '#DDDF0D'], // yellow
//             [(27.49) /35, '#DDDF0D'], // yellow
//             [(27.5) / 35, '#ff7f0e'], // orange
//             [(29.9) / 35, '#ff7f0e'], // orange
//             [(30) / 35, '#d62728'], // red
//             [1, '#d62728'], // red

//         ],
//         lineWidth: 0,
//         tickWidth: 0,
//         minorTickInterval: null,
//         tickAmount: 2,
//         title: {
//             y: 0
//         },
//         labels: {
//             y: 0
//         }
//     },
//     plotOptions: {
//         solidgauge: {
//             innerRadius: '60%',
//             radius: '90%',
//             dataLabels: {
//                 y: 5,
//                 borderWidth: 0,
//                 useHTML: true
//             }
//         }
//     }
// }; // temp

// var gaugeOptions4 = {
//     chart: {
//         type: 'solidgauge',
//         backgroundColor: '#ffffff00',
//         marginTop: 0,
//         marginBottom: 0,
//         marginLeft: 0,
//         marginRight: 0,
//         height: '66%',
//         // width: 470,
//     },
//     title: null,
//     pane: {
//         center: ['50%', '70%'],
//         size: '110%',
//         startAngle: -90,
//         endAngle: 90,
//         background: {
//             backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#e5e5e5',
//             innerRadius: '60%',
//             outerRadius: '90%',
//             shape: 'arc'
//         }
//     },
//     exporting: {
//         enabled: false
//     },
//     tooltip: {
//         enabled: false
//     },
//     yAxis: {
//         plotBands: [{
//                 from: 400,
//                 to: 675,
//                 color: '#1f77b4'
//             },
//             {
//                 from: 675,
//                 to: 900,
//                 color: '#2ca02c'
//             },
//             {
//                 from: 900,
//                 to: 1125,
//                 color: '#DDDF0D'
//             },
//             {
//                 from: 1125,
//                 to: 1350,
//                 color: '#ff7f0e'
//             },
//             {
//                 from: 1350,
//                 to: 1600,
//                 color: '#d62728'
//             },
//         ],
//         stops: [
//             [0., '#1f77b4'], // blue
//             [(674 - 400) / 1200, '#1f77b4'], // blue
//             [(675 - 400) / 1200, '#2ca02c'], // green
//             [(899 - 400) / 1200, '#2ca02c'], // green
//             [(900 - 400) / 1200, '#DDDF0D'], // yellow
//             [(1124 - 400) / 1200, '#DDDF0D'], // yellow
//             [(1125 - 400) / 1200, '#ff7f0e'], //orange
//             [(1349 - 400) / 1200, '#ff7f0e'], //orange
//             [(1350 - 400) / 1200, '#d62728'], // red
//             [(1600 - 400) / 1200, '#d62728'], // red

//         ],
//         lineWidth: 0,
//         tickWidth: 0,
//         minorTickInterval: null,
//         title: {
//             y: 0
//         },
//         labels: {
//             y: 0
//         }
//     },
//     plotOptions: {
//         solidgauge: {
//             innerRadius: '60%',
//             radius: '90%',
//             dataLabels: {
//                 y: 5,
//                 borderWidth: 0,
//                 useHTML: true
//             }
//         }
//     }
// }; // turb

// Oxygen

var chartlineox = Highcharts.chart({
    chart: {
        renderTo: 'container-ox2',
        backgroundColor: '#ffffff00',
        maxHeight: '80%',
        maxWwidth: '100%',
        zoomType: 'x',
    },
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'datetime',
        grid: { // default setting
            enabled: true 
          },
        tickWidth: 1,
   
        title: {
            text: ''
        },
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        }
    },
    yAxis: {
        endOnTick: false,
    
        // min: 0,
        // max: 10,
        title: {
            text: ''
        },
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        }
    },
    series: [{
        showInLegend: false,
        data: [
            [new Date().getTime(), null]
        ],
        zones: [{
            value: 0,
            color: '#d62728'
        }, {
            value: 2.9,
            color: '#d62728'
        }, {
            value: 3,
            color: '#ff7f0e'
        }, {
            value: 3.9,
            color: '#ff7f0e'
        }, {
            value: 4,
            color: '#DDDF0D'
        }, {
            value: 5.9,
            color: '#DDDF0D'
        }, {
            value: 6,
            color: '#2ca02c'
        }, {
            value: 7.9,
            color: '#2ca02c'
        }, {
            value: 8,
            color: '#1f77b4'
        }]
    }]

});

// var gaugeox = Highcharts.chart('container-ox1', Highcharts.merge(gaugeOptions1, {
//     yAxis: {
//         min: 0,
//         max: 10,
//         minorTickLength: 0,
//         lineWidth: 0,
//         tickPixelInterval: 100,
//         tickWidth: 3,
//         tickPosition: 'inside',
//         tickLength: 10,
//         tickColor: '#e5e5e5',
//         tickPositions: [0, 3, 4, 6, 8, 10],
//         labels: {
//             distance: 15,
//             style: {
//                 fontSize: '13px',
//             },
//         },
//         title: {
//             text: ''
//         }
//     },
//     credits: {
//         enabled: false
//     },
//     series: [{
//         name: 'ox',
//         data: [null],
//         dataLabels: {
//             format: '<div style="text-align:center">' +
//                 '<span style="font-size:16px;">{y}</span><br/>' +
//                 '<span style="font-size:12px;opacity:0.4">mg / l</span>' +
//                 '</div>'
//         },
//         tooltip: {
//             valueSuffix: ' '
//         }
//     }],
//     legend: {
//         enabled: true,
//         floating: true,
//         itemWidth: 30,
//         backgroundColor: '#fafafa',
//         color: '#fafafa',
//         x: 0,
//         y: 10,
//         verticalAlign: 'bottom',
//         useHTML: true,
//         itemStyle: {
//             'font-size': '16px',
//             'letter-spacing': '100',
//             'line-height': '0px',
//             'text-align': 'right',
//             "textOverflow": "ellipsis"
//         },

//     },

// }));


// pH

var chartlineph = Highcharts.chart({
    chart: {
        renderTo: 'container-ph2',
        //   type: 'line',
        backgroundColor: '#ffffff00',
        maxHeight: '80%',
        maxWwidth: '100%',
        zoomType: 'x',
    },
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    time: {
        useUTC: false
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'datetime',
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        },
    },
    yAxis: {
        endOnTick: false,
        // min: 4,
        // max: 11,
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        }
    },
    series: [{
        showInLegend: false,
        data: [
            [new Date().getTime(), null]
        ],
        zones: [{
            value: 4.5,
            color: '#ff7f0e'
        }, {
            value: 5.5,
            color: '#ff7f0e'
        }, {
            value: 5.51,
            color: '#DDDF0D'
        }, {
            value: 5.99,
            color: '#DDDF0D'
        }, {
            value: 6,
            color: '#2ca02c'
        }, {
            value: 6.49,
            color: '#2ca02c'
        }, {
            value: 6.5,
            color: '#1f77b4'
        }, {
            value: 8.49,
            color: '#1f77b4'
        }, {
            value: 8.5,
            color: '#2ca02c'
        }, {
            value: 8.99,
            color: '#2ca02c'
        }, {
            value: 9,
            color: '#DDDF0D'
        }, {
            value: 9.49,
            color: '#DDDF0D'
        }, {
            value: 9.5,
            color: '#ff7f0e'
        }, {
            value: 11,
            color: '#ff7f0e'
        }, ]
    }]
});

// var gaugeph = Highcharts.chart('container-ph1', Highcharts.merge(gaugeOptions2, {
//     yAxis: {
//         min: 4.5,
//         max: 10.5,
//         minorTickLength: 0,
//         lineWidth: 0,
//         tickPixelInterval: 100,
//         tickWidth: 3,
//         tickPosition: 'inside',
//         tickLength: 10,
//         tickColor: '#e5e5e5',
//         tickPositions: [4.5, 5.5, 6, 6.5, 8.5, 9, 9.5, 10.5],
//         labels: {
//             distance: 15,
//             style: {
//                 fontSize: '13px',
//             },
//         },
//         title: {
//             text: ''
//         }
//     },
//     credits: {
//         enabled: false
//     },
//     series: [{
//         name: 'Speed',
//         data: [null],
//         dataLabels: {
//             format: '<div style="text-align:center">' +
//             '<span style="font-size:16px;">{y}</span><br/>' +
//             '<span style="font-size:12px;opacity:0.4">pH</span>' +
//             '</div>'
//     },
//         tooltip: {
//             valueSuffix: ' '
//         }
//     }]

// }));

// temp

var chartlinetemp = Highcharts.chart({
    chart: {
        renderTo: 'container-temp2',
        type: 'line',
        backgroundColor: '#ffffff00',
        maxHeight: '80%',
        maxWwidth: '100%',
        zoomType: 'x',
    },
    exporting: {
        enabled: false
    },
    credits: {
        enabled: false
    },
    time: {
        useUTC: false
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'datetime',
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        },
    },
    yAxis: {
        // min: 15, 
        // max: 35,
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        }
    },
    rangeSelector: {
        buttons: [{
            text: '+',
            events: {
                click() {
                    return false
                }
            }
        }, {
            text: '-',
            events: {
                click() {
                    return false
                }
            }

        }]
    },
    series: [{
        showInLegend: false,
        data: [
            [new Date().getTime(), null]
        ],
        zones: [{
            value: 15,
            color: '#1f77b4'
        }, {
            value: 22.99,
            color: '#1f77b4'
        }, {
            value: 23,
            color: '#2ca02c'
        }, {
            value: 24.99,
            color: '#2ca02c'
        }, {
            value: 25,
            color: '#DDDF0D'
        }, {
            value: 27.49,
            color: '#DDDF0D'
        }, {
            value: 27.5,
            color: '#ff7f0e'
        }, {
            value: 29.99,
            color: '#ff7f0e'
        }, {
            value: 30,
            color: '#d62728'
        }, {
            value: 35,
            color: '#d62728'
        }, ]
    }]
});

// var gaugetemp = Highcharts.chart('container-temp1', Highcharts.merge(gaugeOptions3, {
//     yAxis: {
//         min: 0,
//         max: 35,
//         minorTickLength: 0,
//         lineWidth: 0,
//         tickPixelInterval: 30,
//         tickWidth: 3,
//         tickPosition: 'inside',
//         tickLength: 10,
//         tickColor: '#e5e5e5',
//         tickPositions: [0, 23, 25, 27.5, 30, 35],
//         labels: {
//             distance: 15,
//             style: {
//                 fontSize: '13px',
//             },
//         },
//         title: {
//             text: ''
//         }
//     },
//     credits: {
//         enabled: false
//     },
//     series: [{
//         name: 'Speed',
//         data: [
//             [null]
//         ],
//         dataLabels: {
//             format: '<div style="text-align:center">' +
//             '<span style="font-size:16px;">{y}</span><br/>' +
//             '<span style="font-size:12px;opacity:0.4">°C</span>' +
//             '</div>'
//         },
//         tooltip: {
//             valueSuffix: ' '
//         }
//     }]

// }), function(chartt) { // on complete

//     // if (gaugetemp.series.length >3) {
//     //    var img = chartt.renderer.image('./img/3_fish.png', 20, 220, 50, 50).add()
//     // };


//     // chartt.renderer.image('./img/2_fish.png', 80, 90,  50, 50).add();
//     // chartt.renderer.image('./img/1_fish.png', 270, -5, 50, 50).add();
//     // chartt.renderer.image('./img/1_dead_fish.png', 350, -5, 80, 80).add();
//     // chartt.renderer.image('./img/2_dead_fish.png', 430, 50,  80, 80).add();
//     // chartt.renderer.image('./img/3_dead_fish.png', 500, 140, 80, 80).add();
// });


// conductivity

var chartlineturb = Highcharts.chart({
    chart: {
        renderTo: 'container-turb2',
        // type: 'line',
        backgroundColor: '#ffffff00',
        maxHeight: '80%',
        maxWwidth: '100%',
        zoomType: 'x',
    },
    credits: {
        enabled: false
    },
    exporting: {
        enabled: false
    },
    time: {
        useUTC: false
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'datetime',
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        },
    },
    yAxis: {
        // min: 400,
        // max: 1700,
        labels: {
            useHTML: true,
            style: {
                fontSize: '13px'
            }
        },
        title: {
            text: ''
        }
    },
    series: [{
        showInLegend: false,
        data: [
            [new Date().getTime(), null]
        ],
        zones: [{
            value: 400,
            color: '#1f77b4'
        }, {
            value: 674.99,
            color: '#1f77b4'
        }, {
            value: 675,
            color: '#2ca02c'
        }, {
            value: 899.99,
            color: '#2ca02c'
        }, {
            value: 900,
            color: '#DDDF0D'
        }, {
            value: 1124.99,
            color: '#DDDF0D'
        }, {
            value: 1125,
            color: '#ff7f0e'
        }, {
            value: 1349.99,
            color: '#ff7f0e'
        }, {
            value: 1350,
            color: '#d62728'
        }, {
            value: 1600,
            color: '#d62728'
        }, ]
    }]
});

// var gaugeturb = Highcharts.chart('container-turb1', Highcharts.merge(gaugeOptions4, {
//         yAxis: {
//             min: 400,
//             max: 1600,
//             minorTickLength: 0,
//             lineWidth: 0,
//             tickPixelInterval: 30,
//             tickWidth: 3,
//             tickPosition: 'inside',
//             tickLength: 10,
//             tickColor: '#e5e5e5',
//             tickPositions: [400, 675, 900, 1125, 1350, 1600],
//             labels: {
//                 distance: 15,
//                 style: {
//                     fontSize: '13px',
//                 },
//             },
//             title: {
//                 text: ''
//             }
//         },
//         credits: {
//             enabled: false
//         },
//         series: [{
//             name: 'Speed',
//             data: [null],
//             dataLabels: {
//                 format: '<div style="text-align:center">' +
//                 '<span style="font-size:16px;">{y}</span><br/>' +
//                 '<span style="font-size:12px;opacity:0.4">&mu; S /cm</span>' +
//                 '</div>'
//             },
//             tooltip: {
//                 valueSuffix: ' '
//             }
//         }]

//     }),
//     //   function(chartt) { // on complete

//     //     chartt.renderer.image('./img/dead_fish.png', 40, 240, 40, 40).add();
//     //     chartt.renderer.image('./img/dead_fish.png', 90, 90, 40, 40).add();
//     //     chartt.renderer.image('./img/fish.png', 270, 10, 40, 40).add();
//     //     chartt.renderer.image('./img/fish.png', 460, 90, 40, 40).add();
//     //     chartt.renderer.image('./img/fish.png', 520, 240, 40, 40).add();
//     //   }
// );

// update plots

var i1, i2, i3, i4;
i1 = 0;
i2 = 0;
i3 = 0
i4 = 0;


setInterval(function() {
    var xmin, q1, q2, q3, q4, img1, img2, img3, img4, img5;
    img1 = document.getElementById("myimg1");
    img2 = document.getElementById("myimg2");
    img3 = document.getElementById("myimg3");
    img4 = document.getElementById("myimg4");
    img5 = document.getElementById("myimg5");

    $.getJSON('https://monitoring.smartwater.brussels/grafana/api/datasources/proxy/4/query?db=smartwater_sewer&q=SELECT%20mean(%22value%22)%20FROM%20%22device_frmpayload_data_dissolved_oxygen%22%20WHERE%20time%20%3E%3D%20now()%20-%202d%20and%20time%20%3C%3D%20now()%20-%201m%20GROUP%20BY%20time(10m)%2C%20%22device_name%22%20fill(none)&epoch=ms',
        function(dataox) {
            i1 = i1 + 1;
            if (i1 > -1) {
                chartlineox.series[0].setData(dataox.results[0].series[0].values);
            };
            // x = data.results[0].series[0].values[data.results[0].series[0].values.length-1][0];
            x = new Date().getTime();
            y1 = dataox.results[0].series[0].values[dataox.results[0].series[0].values.length - 1][1];
            chartlineox.series[0].setData(dataox.results[0].series[0].values);

            if (gaugeox) {
                gaugeox.set(y1);

            }
            if (chartlineox) {
                if (chartlineox.series[0].length < 10) {
                    chartlineox.series[0].addPoint([x, y1], true, false);
                } else {
                    chartlineox.series[0].addPoint([x, y1], true, true);
                }
            }
            // console.log(dataox.results[0].series[0].values)
        });
    $.getJSON('https://monitoring.smartwater.brussels/grafana/api/datasources/proxy/4/query?db=smartwater_sewer&q=SELECT%20mean(%22value%22)%20FROM%20%22device_frmpayload_data_pH%22%20WHERE%20time%20%3E%3D%20now()%20-%202d%20and%20time%20%3C%3D%20now()%20-%201m%20GROUP%20BY%20time(10m)%2C%20%22device_name%22%20fill(none)&epoch=ms',
        function(dataph) {
            i2 = i2 + 1;
            if (i2 > -1) {
                chartlineph.series[0].setData(dataph.results[0].series[0].values);
            };
            // x = data.results[0].series[0].values[data.results[0].series[0].values.length-1][0];
            x = new Date().getTime();
            y2 = dataph.results[0].series[0].values[dataph.results[0].series[0].values.length - 1][1];
            chartlineph.series[0].setData(dataph.results[0].series[0].values);

            if (gaugeph) {
                gaugeph.set(y2);

            }
            if (chartlineph) {
                if (chartlineph.series[0].length < 10) {
                    chartlineph.series[0].addPoint([x, y2], true, false);
                } else {
                    chartlineph.series[0].addPoint([x, y2], true, true);
                }
            }
            // console.log(dataph.results[0].series[0].values)
        });
    $.getJSON('https://monitoring.smartwater.brussels/grafana/api/datasources/proxy/4/query?db=smartwater_sewer&q=SELECT%20mean(%22value%22)%20FROM%20%22device_frmpayload_data_temperature%22%20WHERE%20time%20%3E%3D%20now()%20-%202d%20and%20time%20%3C%3D%20now()%20-%201m%20GROUP%20BY%20time(10m)%2C%20%22device_name%22%20fill(none)&epoch=ms',
        function(datat) {
            i3 = i3 + 1;
            if (i3 > -1) {
                chartlinetemp.series[0].setData(datat.results[0].series[0].values);
            };
            // x = data.results[0].series[0].values[data.results[0].series[0].values.length-1][0];
            x = new Date().getTime();
            if (i3 == 1) {}
            y3 = datat.results[0].series[0].values[datat.results[0].series[0].values.length - 1][1];
            chartlinetemp.series[0].setData(datat.results[0].series[0].values);

            if (gaugete) {
                gaugete.set(y3);

            }
            if (chartlinetemp) {
                if (chartlinetemp.series[0].length < 10) {
                    chartlinetemp.series[0].addPoint([x, y3], true, false);
                } else {
                    chartlinetemp.series[0].addPoint([x, y3], true, true);
                }
            }
            // console.log(datat.results[0].series[0].values)
        });
    $.getJSON('https://monitoring.smartwater.brussels/grafana/api/datasources/proxy/4/query?db=smartwater_sewer&q=SELECT%20mean(%22value%22)%20FROM%20%22device_frmpayload_data_conductivity%22%20WHERE%20time%20%3E%3D%20now()%20-%202d%20and%20time%20%3C%3D%20now()%20-%201m%20GROUP%20BY%20time(5m)%2C%20%22device_name%22%20fill(none)&epoch=ms',
        function(datac) {
            i4 = i4 + 1;
            if (i4 > -1) {
                chartlineturb.series[0].setData(datac.results[0].series[0].values);
            };
            // x = data.results[0].series[0].values[data.results[0].series[0].values.length-1][0];
            x = new Date().getTime();
            y4 = datac.results[0].series[0].values[datac.results[0].series[0].values.length - 1][1];
            chartlineturb.series[0].setData(datac.results[0].series[0].values);

            if (gaugeco) {
                gaugeco.set(y4);

            }
            if (chartlineturb) {
                if (chartlineturb.series[0].length < 10) {
                    chartlineturb.series[0].addPoint([x, y4], true, false);
                } else {
                    chartlineturb.series[0].addPoint([x, y4], true, true);
                }
            }
            // console.log(datac.results[0].series[0].values)
        });
    if (y1 >= 8) {
        q1 = 5;
    } else if (y1 < 8 && y1 >= 6) {
        q1 = 4;
    } else if (y1 < 6 && y1 >= 4) {
        q1 = 3;
    } else if (y1 < 4 && y1 >= 3) {
        q1 = 2;
    } else {
        q1 = 1;
    }
    if (y2 >= 6.5 && y2 <= 8.5) {
        q2 = 5;
    } else if (y2 < 6.5 && y2 >= 6 || y2 > 8.5 && y2 <= 9) {
        q2 = 4;
    } else if (y2 < 6 && y2 >= 5.5 || y2 > 9 && y2 <= 9.5) {
        q2 = 3;
    } else {
        q2 = 2;
    }
    if (y3 <= 23) {
        q3 = 5;
    } else if (y3 > 23 && y3 <= 25) {
        q3 = 4;
    } else if (y3 > 25 && y3 <= 27.5) {
        q3 = 3;
    } else if (y3 > 27.5 && y3 <= 30) {
        q3 = 2;
    } else {
        q3 = 1;
    }
    if (y4 <= 675) {
        q4 = 5;
    } else if (y4 > 675 && y4 <= 900) {
        q4 = 4;
    } else if (y4 > 900 && y4 <= 1125) {
        q4 = 3;
    } else if (y4 > 1125 && y4 <= 1350) {
        q4 = 2;
    } else {
        q4 = 1;
    }

    // xmin = Math.min(q1, q2, q3, q4);
    xmin = Math.min(q2, q3, q4);
    console.log(y1, q1, y2, q2, y3, q3, y4, q4, xmin);
    if (xmin == 5) {
        // msg1.style.display = "block";
        // msg2.style.display = 'none';
        // msg3.style.display = 'none';
        // msg4.style.display = 'none';
        // msg5.style.display = 'none';
        img1.style.display = 'block';
        img2.style.display = 'none';
        img3.style.display = 'none';
        img4.style.display = 'none';
        img5.style.display = 'none';
    } else if (xmin == 4) {
        // msg1.style.display = 'none';
        // msg2.style.display = 'block';
        // msg3.style.display = 'none';
        // msg4.style.display = 'none';
        // msg5.style.display = 'none';
        img1.style.display = 'none';
        img2.style.display = 'block';
        img3.style.display = 'none';
        img4.style.display = 'none';
        img5.style.display = 'none';

    } else if (xmin == 3) {
        // msg1.style.display = 'none';
        // msg2.style.display = 'none';
        // msg3.style.display = 'block';
        // msg4.style.display = 'none';
        // msg5.style.display = 'none';
        img1.style.display = 'none';
        img2.style.display = 'none';
        img3.style.display = 'block';
        img4.style.display = 'none';
        img5.style.display = 'none';

    } else if (xmin == 2) {
        // msg1.style.display = 'none';
        // msg2.style.display = 'none';
        // msg3.style.display = 'none';
        // msg4.style.display = 'block';
        // msg5.style.display = 'none';
        img1.style.display = 'none';
        img2.style.display = 'none';
        img3.style.display = 'none';
        img4.style.display = 'block';
        img5.style.display = 'none';

    } else if (xmin == 1) {
        // msg1.style.display = 'none';
        // msg2.style.display = 'none';
        // msg3.style.display = 'none';
        // msg4.style.display = 'none';
        // msg5.style.display = 'block';
        img1.style.display = 'none';
        img2.style.display = 'none';
        img3.style.display = 'none';
        img4.style.display = 'none';
        img5.style.display = 'block';

    }


}, 1000 * 30);
gaugeox.setTextField(document.getElementById("gauge1-txt"), 2);
gaugeph.setTextField(document.getElementById("gauge2-txt"), 2);
gaugete.setTextField(document.getElementById("gauge3-txt"), 2);
gaugeco.setTextField(document.getElementById("gauge4-txt"), 2);

// slideshows fr/nl/en

$("#slideshow > div:gt(0)").hide();

setInterval(function() {
    $('#slideshow > div:first')
        .fadeOut(200)
        .next()
        .fadeIn(200)
        .end()
        .appendTo('#slideshow');
},  1000 * 60 * 2);

$("#slideshow2 > div:gt(0)").hide();

setInterval(function() {
    $('#slideshow2 > div:first')
        .fadeOut(200)
        .next()
        .fadeIn(200)
        .end()
        .appendTo('#slideshow2');
},  1000 * 60 * 2);

$("#slideshow3 > div:gt(0)").hide();

setInterval(function() {
    $('#slideshow3 > div:first')
        .fadeOut(200)
        .next()
        .fadeIn(200)
        .end()
        .appendTo('#slideshow3');
},  1000 * 60 * 2);

$("#slideshow4 > div:gt(0)").hide();

setInterval(function() {
    $('#slideshow4 > div:first')
        .fadeOut(200)
        .next()
        .fadeIn(200)
        .end()
        .appendTo('#slideshow4');
},  1000 * 60 * 2);

$("#slideshow5 > div:gt(0)").hide();

setInterval(function() {
    $('#slideshow5 > div:first')
        .fadeOut(200)
        .next()
        .fadeIn(200)
        .end()
        .appendTo('#slideshow5');
},  1000 * 60 * 2);


$("#slideshow-counter > div:gt(0)").hide();

setInterval(function() {
    $('#slideshow-counter > div:first')
        .fadeOut(200)
        .next()
        .fadeIn(200)
        .end()
        .appendTo('#slideshow-counter');
},  1000 * 60 * 2);

$("#slideshow-footer > div:gt(0)").hide();

setInterval(function() {
    $('#slideshow-footer > div:first')
        .fadeOut(200)
        .next()
        .fadeIn(200)
        .end()
        .appendTo('#slideshow-footer');
},  1000 * 60 * 2);


$("#slideshow-header > div:gt(0)").hide();

setInterval(function() {
    $('#slideshow-header > div:first')
        .fadeOut(200)
        .next()
        .fadeIn(200)
        .end()
        .appendTo('#slideshow-header');
},  1000 * 60 * 2);

// clock
function pad(num) {
    return String("0" + num).slice(-2);
}

function updateClock() {
    var now = new Date();
    var time = pad(now.getHours()) + ':' + pad(now.getMinutes());
    var day = pad(now.getDate()) + "/" + pad(now.getMonth() + 1) + "/" + (now.getFullYear());
    document.getElementById('current').innerHTML = day + " - " + time;
}
window.onload = function() {
    setInterval(updateClock, 1000*60); // use a shorter interval for better update
}

// countdown

// Income Ticker Display (displaying time until next pay day)
var incomeTicker = 121;

window.setInterval(function() {
    if (incomeTicker > 0)
        incomeTicker--;
    if (incomeTicker > 30 && incomeTicker % 5 == 0) {
        document.getElementById("timer").innerHTML = incomeTicker;
    };
    if (incomeTicker <= 30){
        document.getElementById("timer").innerHTML = incomeTicker;
    };
    if (incomeTicker <= 0){
        incomeTicker = 121;
    }
}, 1000);
